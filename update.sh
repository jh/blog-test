#!/bin/bash
# Updates this repository as well as all Git submodules
set -e;

git pull;

git submodule update --init --recursive;
