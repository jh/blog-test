#!/bin/sh

# Officially "recommended" image:
# https://hub.docker.com/r/klakegg/hugo/

podman run --rm -it \
     -v "${PWD}:/src" \
     -p 1313:1313 \
     docker.io/klakegg/hugo:0.75.1-ext-alpine \
     server \
     --verbose \
     --noHTTPCache=true \
     --buildDrafts --buildFuture --buildExpired \
     --watch
