# Jack's Blog

[![Build Status](https://ci.cubieserver.de/api/badges/jh/blog/status.svg)](https://ci.cubieserver.de/jh/blog)

Versioned by [Gitea](https://gitea.io/), Built by [Woodpecker](https://woodpecker-ci.org/), Rendered by [Hugo](https://gohugo.io), Stored in [Minio](https://minio.io/), Cached by [Nginx](http://nginx.org/), Served by [Traefik](https://traefik.io/).

## Building
Simply run `$ hugo` in the main directory (this will create a `public/` folder with the web files)

Alternatively, use the [Docker Image jacksgt/hugo](https://hub.docker.com/r/jacksgt/hugo) ([https://github.com/jacksgt/docker-hugo](https://github.com/jacksgt/docker-hugo)) which is used by the CI/CD server to render the blog.

The CI/CD server will run a build on each push, but only publish the artifacts of the master branch. To skip a CI build (i.e. not run an integration test for this specific commit, e.g. because I doesn't modify any content), use `[CI SKIP]` in the commit message.

## Images

Command for compressing and optimizing header background image (from <https://dev.to/feldroy/til-strategies-for-compressing-jpg-files-with-imagemagick-5fn9>):
```
convert orig.jpg \
-resize 50% \
-sampling-factor 4:2:0 \
-strip \
-quality 90 \
-interlace JPEG \
-colorspace RGB \
compressed.jpg
```

Create JPG thumbnail ("poster") for video (from <https://stackoverflow.com/a/68705261>):
```
ffmpeg -ss 00:01:00 -i $video -vf 'scale=1280:720' -vframes 1 ${video}.jpg
```

## Videos

Trim / cut video from start timestamp (`ss`) to timestamp (`to`) (from <https://stackoverflow.com/a/42827058>):

```
ffmpeg -ss 00:01:00 -to 00:02:00 -i input.mp4 -c copy output.mp4
```
