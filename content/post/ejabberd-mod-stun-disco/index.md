+++
title = "ejabberd: Announce external STUN/TURN over XMPP (XEP-0215)"
Description = "How to configure ejabberd to announce an external STUN and TURN server with XEP-0215 (External Service Discovery)"
Tags = ["xmpp", "ejabberd", "conversations"]
categories = "Software"
date = "2020-05-10"
+++

Since [Conversations release 2.8](https://github.com/iNPUTmice/Conversations/releases/tag/2.8.0), audio and video calling is supported. This feature is an amazing addition simply amazing makes Conversations equivalent to other major chat apps.

To get this feature working however, you need a server which has STUN and TURN services exposed and also announces them via XMPP ([XEP-0215](https://xmpp.org/extensions/xep-0215.html)), so Conversations can pick up the configuration and use them.

There is a [nice guide on how to set up Prosody and coTURN](https://prosody.im/doc/coturn), and the author of Conversations posted some [minimal instructions for using ejabberd's built-in STUN/TURN server](https://gist.github.com/iNPUTmice/a28c438d9bbf3f4a3d4c663ffaa224d9), but I couldn't figure out how to announce an *external* STUN/TURN server via ejabberd.

First, I was looking into [mod_disco](https://docs.ejabberd.im/admin/configuration/modules/#mod-disco), which is ejabberd implementation for service discovery.
Unfortunately, this led me nowhere, since it only implements [XEP-0030](https://xmpp.org/extensions/xep-0030.html) ("Service Discovery"), not XEP-0215 ("External Service Discovery").

> This module adds support for XEP-0030: Service Discovery. With this module enabled, services on your server can be discovered by XMPP clients.

It turns out that since release 20.04 [ejabberd has a new module](https://github.com/processone/ejabberd/commit/69d1d62add92511acf58f5f71383ad64e4757464) to handle exactly this functionality: [mod_stun_disco](https://docs.ejabberd.im/admin/configuration/modules/#mod-stun-disco).

> This module allows XMPP clients to discover STUN/TURN services and to obtain temporary credentials for using them as per XEP-0215: External Service Discovery.

Since ejabberd has a built-in module for STUN and TURN services, you can either use that one, or use an external STUN / TURN server such as [coTURN](https://github.com/coturn/coturn).

My coTURN server is setup as [described here](https://help.nextcloud.com/t/howto-setup-nextcloud-talk-with-turn-server/30794).
The following configuration assumes your STUN and TURN server are running on the default port (3478) and are not using the encrypted variants (`stuns` and `turns`).
Furthermore, it assumes the default transport protocol is UDP, which is true in most cases.

```yaml
modules:
  mod_stun_disco:
    secret: y0ur-$hared-$3cR3t-here
    services:
      -
        host: example.com
        type: stun
      -
        host: example.com
        type: turn
```

If you have a typical STUN / TURN server setup (e.g. for Jitsi Meet or Nextcloud Talk), your STUN server allows anonymous login and your TURN server requires authentication. That's what the secret shown above is used for.

For the detailed setup, please refer to the [ejabberd documentation on mod_stun_disco](https://docs.ejabberd.im/admin/configuration/modules/#mod-stun-disco).

I hope this helped you set up your ejabberd server for audio and video calls with Conversations. Happy calling!
