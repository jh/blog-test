+++
title = "Route des Grandes Alpes"
description = "My experience of cycling the Route des Grandes Alpes from Geneva to Nice in 2023"
categories = "Outdoor"
tags = ["cycling", "geneva", "nice"]
date = "2023-09-08"
+++

At the end of August 2023 I cycled along the *Route des Grandes Alpes* (*"route of the high alps"*) from Geneva to Nice.
The route is quite famous for cyclist and motorbikes alike and has a [dedicated website](https://routedesgrandesalpes.com/) that describes several itineraries.

{{< figure src="sign-route-des-grandes-alpes.jpg" alt="Street sign for the 'Route des Grandes Alpes'" width="60%">}}

All of the variants pass by iconic *Tour de France* and *Giro d'Italia* climbs such as *Col du Galibier*, *Alpe d'Huez* or *Col de la Croix de Fer* ([Climb List: Legendary Climbs of the Tour de France](https://pjammcycling.com/zone/188.Tour-de-France---Legendary-Climbs)).
The cycling races have passed these many times and they were often the showground of epic battles (for example: [Patani vs. Ulrich at Col du Galibier [1998]](https://www.youtube.com/watch?v=h2FQqHF8x5I)).

All of this to say that no matter which itinerary you do, it will most certainly be a wonderful adventure.

In my case, I put together the following route:

* Day 1: Geneva - Albertville (115km)
* Day 2: Albertville - Briancon (161km)
* Day 3: Briancon - Barcelonette (101km)
* Day 4: Barcelonette - Roquebiliere (130km)
* Day 5: Roquebiliere - Nice / Antibes (113km)

## Day 1

On the first day I was lucky enough to be able to start directly at my doorstep in Geneva.
I passed the iconic flower clock at the shore of *Lac Léman* and (slowly) started escaping the morning chaos in the city (slowly, mind you).

{{< figure alt="The iconic flower clock in Geneva at the shore of Lac Léman" src="day-1-geneva-flower-clock.jpg" width="60%" >}}

In the picture above you can also see the only equipment I had with me for the week: a small backpack with some spare clothes and shoes, bathroom supplies, electronic chargers - plus the cycling gear I was wearing.
My bike for this week was a [BMC Teammachine SLR]({{< ref "bmc-teammachine" >}}).

Usually the *Route des Grandes Alpes* starts in *Thonon-les-Bains* and takes in a couple of cols (mountain passes) of the *Chablais Mountains*.
Since I'm already familiar with that area, I skipped this part and followed along the river *Arve* to *Bonneville* - this way I would have more energy for the rest of my journey.
Shortly after, the [16 kilometers climb to the *Col de la Colombière*](https://climbfinder.com/en/climbs/col-de-la-colombiere-scionzier) started.


{{< figure src="day-1-le-reposoir.jpg" caption="Climbing through the village Le Reposoir, looking towards the Aravis massif" width="60%" >}}

The peaceful setting of this climb set a wonderful atmosphere for the rest of the trip.

{{< figure src="day-1-col-de-la-colombiere.jpg" caption="At the top of the Col de la Colombière" width="60%" >}}

<!-- The descent from the Col de la Colombière down to Le Grand Bonnard was quite enjoyable. -->
<!-- WHY??? -->

I did my lunch stop in La Clusaz to refuel before tackling the *Col des Aravis* in the early afternoon.
It's not a very long [climb from La Clusaz](https://climbfinder.com/en/climbs/col-des-aravis-thones), but there are no trees to provide any shade so the 8 kilometers were quite intense in the midday heat.

{{< figure src="day-1-col-des-aravis.jpg" alt="At the top of Col des Aravis" width="60%" >}}

At the top I was rewarded with a view onto the highest mountain of the Alpes: the *Mont Blanc*.

{{< figure src="day-1-col-des-aravis-mont-blanc.jpg" alt="View onto the Mont Blanc from Col des Aravis" width="60%" >}}

The descent from Col des Aravis to *La-Giettaz-en-Arravis* was very technical with lots of small turns and twists while at the same time having a good amount of fast flowing sections - highly recommended!
From there onward I followed the valley floor to *Albertville*, where I relaxed and cooled down at the artificial excavation lake *Gringon*.

{{< figure src="day-1-albertville-lake.jpg" alt="Relaxing at the lake in Albertville" width="60%" >}}

{{< map gpxfile="day-1-to-albertville.gpx" show_waypoints="false" >}}


<!-- Col de la Colombiere (1604m) -->
<!-- Col des Aravis (1487m) -->

---

## Day 2

I started my journey early the next morning because the second day was a big day: 160 kilometers with 4500 meters of vertical elevation were waiting for me.

{{< figure src="day-2-sunrise-albertville.jpg" alt="Sunrise in Albertville" caption="Sunrise in Albertville" width="60%" >}}

On the first 20 kilometers I could gently warm up my legs while cycling along the *Isère* valley, before starting the first big climb of the day: [*Col de la Madeleine* from Feissons-sur-Isère](https://climbfinder.com/en/climbs/col-de-la-madeleine-feissons-sur-isere).
This infamous Tour de France climb is 25 kilometers long and has an average gradient of 6% - but the average is deceptively low because in the middle there are two flat kilometers, whereas most kilometers are between 7 and 10 percent gradient.
Nevertheless, I can assure you that the climb is totally worth it: a small and quiet road, sleepy mountain villages, surrounded by beautiful scenery - the ingredients for a perfect climb.

{{< figure src="day-2-climb-col-de-la-madeleine-1.jpg" alt="Quiet road on the way to Col de la Madeleine" width="60% ">}}

{{< figure src="day-2-climb-col-de-la-madeleine-2.jpg" alt="Beautiful landscape on the way to Col de la Madeleine" width="60% ">}}

Since this was the first high altitude col of the journey, the landscape, the depth, the view were almost frighteningly touching.

{{< figure src="day-2-col-de-la-madeleine.jpg" alt="At the top of Col de la Madeleine" width="60% ">}}

After descending down towards *La Chambre* - another beautiful descent with a healthy mix of technical twists and fast flowing corners - I stopped for lunch in *Saint-Jean-de-Maurienne*.
There are a lot of different lunch options available in this small town, the only difficulty was finding one that would serve me food at 11 am.
But I couldn't wait any longer since the second big challenge of the day awaited me: the [*Col du Telegraphe* from *Saint-Michel-de-Maurienne*](https://climbfinder.com/en/climbs/col-du-telegraphe) followed by the mighty [*Col du Galibier*](https://climbfinder.com/en/climbs/col-du-galibier) - 34 kilometers in total!


{{< figure src="day-2-col-du-telegraphe.jpg" alt="At the top of Col du Telegraphe" width="60%" >}}

{{< figure src="day-2-on-the-way-to-galibier.jpg" alt="Road towards the Galibier" width="60%" >}}

Climbing the last 15 kilometers from *Vailore* to the Col du Galibier was a tough challenge: not only did I already have significant kilometers and altitude in my legs, also the afternoon sun and heat made it even more challenging - because above 2000 meters you won't find any type of shade anymore.

{{< figure src="day-2-col-du-galibier.jpg" alt="At the top of Col du Galibier" width="60%" >}}

Overall, I would even say I was somewhat disappointed by this climb.
The wide, two laned road (that goes all the way up to the col) was pretty busy with cars, motorcycles, campers and trucks (this was still in August during summer vacation season, mind you), so it was quite a different feeling compared to the peaceful *Col de la Madeleine* in the morning.

I also found the the descent from Col du Galibier via *Col du Lautaret* to *Briançon* to be quite dull - the fact that it was going downhill all the way was definitely the "best" part, since my legs were quite toast after this long day.

{{< map gpxfile="day-2-to-briancon.gpx" show_waypoints="false" >}}

<!-- Col de la Madelaine (2000m) -->
<!-- Col du Télégraphe (1566m) -->
<!-- Col du Galibier (2642m) -->
<!-- Col du Lautaret (2058m) -->

---

## Day 3

The third day was intended to be somewhat of a recovery day.
Those familiar with the geography of Briançon will certainly understand that no matter which direction you exit the town, you immediately start going upwards.
Thus my legs did not get any time for a gentle warmup, instead I headed straight for my first climb of the day: [*Col d'Izoard*](https://climbfinder.com/en/climbs/col-d-izoard-briancon).
Luckily, the first few kilometers are still somewhat "gentle" (3-5%), before I hit the last 8 kilometers with an average gradient of 8%.

{{< figure src="day-3-leaving-briancon.jpg" caption="Climbing out of Briançon alongside fellow cyclist" width="60%" >}}

This was one of the most enjoyable and rewarding the climbs I have done so far.
It is definitely challenging, but never crazy hard.
The road is in pristine condition and yet there are not many other vehicles going around, so you can really enjoy the peace of nature and focus on your rhythm.
The countless hairpin turns towards the end of the climb allow many different views onto the beautiful scenery.

{{< figure src="day-3-col-d-izoard.jpg" alt="The monument at the top of Col d'Izoard" width="60%" >}}

Also the descent from Col d'Izoard towards *Guillestre* is very nice.
The first part is quite fast and technical - even dangerous since you're riding right next to the abyss.
Once you reach *Guil* valley, the road follows along the impressive gorge the *Guil* creek has carved into the mountains.

{{< figure src="day-3-guil-gorge.jpg" caption="The gorge of the river Guil" width="60%" >}}

At this point I really noticed that the vegetation in the mountains became more "mediterranean" - even though I'm still far away from the sea!

After a couple of kilometers of resting the legs while following the valley, the next climb began: [*Col du Vars* from *Guillestre*](https://climbfinder.com/en/climbs/col-de-vars-guillestre).

{{< figure src="day-3-saint-marcellin.jpg" caption="Refilling my water bottles on the way to Col du Vars" width="60%" >}}

Apart from the fact that the first kilometers are quite tough, there's really nothing special to say about this climb in my opinion - despite the altitude, not even the summit is impressive.

{{< figure src="day-3-col-du-vars.jpg" alt="At the summit of Col du Vars" width="60%" >}}

At the end of my third day an amusing accommodation awaited me:

{{< figure src="day-3-accommodation-barcelonette.jpg" alt="Cabin accommodation in Barcelonette" width="60%" >}}

I found this little cabin on the [camping site "Le Tampico"](https://www.letampico.fr/) in *Barcelonette*.
They have many other interesting types of accommodation available, so if you are in the area I highly recommend to stay there for a night.
Oh, let me not forget to mention they also have a charming restaurant with a wood fired pizza oven onsite!

{{< map gpxfile="day-3-to-barcelonette.gpx" show_waypoints="false" >}}

<!-- Col d'Izoard (2360m) -->
<!-- Col du Vars (2109m) -->

---

## Day 4

Albeit shorter than the second day, the fourth day was again a big day because I reached the highest point of my journey: 2802 meters above sea level.
After roughly ten kilometers of warmup on flat road, I reached the village *Jausiers* from where the [climb to the *Cime de la Bonette*](https://climbfinder.com/en/climbs/col-de-la-bonette-jausiers) starts - which is marketed as "the highest through road in the Alps".

{{< figure src="day-4-jausiers.jpg" alt="Street sign in Jausiers" width="60%" >}}

Needless to say that this is a loooong climb: 23 kilometers consistently between 6 and 8 percent gradient, starting at 1220 meters and finishing at 2802 meters above sea level.
At the same time this length and altitude difference also makes it a real journey: first you are cycling through typical mountain agricultural farmland, followed by forest sections and impressive boulders.
Eventually you emerge into a surreal "mars landscape" that is solely dominated by rock covered mountains.

{{< figure src="day-4-climb-to-bonette.jpg" caption="The mars-like landscape surround the Bonette (in the center) - you can see that the last kilometer around the mountain is quite steep" width="60%" >}}

And then finally you reach the Col de la Bonette at 2715 m - but the final kilometer around the mountain has gradients up to 15% (average of 10%) which really hurts after such a long climb and especially at this altitude.
However anyone who made it this far will surely still make it to the top of the mountain (2802m).

{{< figure src="day-4-col-de-la-bonette.jpg" caption="The final, steep kilometer to the Cime de la Bonette" width="60%" >}}

{{< figure src="day-4-cime-de-la-bonette.jpg" caption="Cime de la Bonette" width="60%" >}}

In theory, from here on it was downhill for a long while, in practice however I had a strong headwind coming up the *Tineé* valley which meant I still had to pedal with my tired legs.

{{< figure src="day-4-isola.jpg" caption="A tiny street in Isola with a bike for size comparison" width="60%" >}}

I made my lunch stop in village *Isola*, before approaching the last climb of the day: [Col du Saint Martin from the Tineé valley](https://climbfinder.com/en/climbs/col-saint-martin-col-colmiane-la-bouline).
<!-- It's a 16 kilometers climb that bridges roughly 1000 meters of altitude. -->

{{< figure src="day-4-tdf-sign-col-st-martin.jpg" alt="Tour de France sign for the climb to Col du Saint Martin" width="60%" >}}

Again, since I did this climb around midday, the heat from the asphalt and burning sun was a significant challenge.
Nevertheless I found this to be quite an enjoyable climb with lots of beautiful scenery around.
Luckily for my tired legs, the gradients on this climb are never too steep - as long as you keep going, you'll eventually make it to the top!

{{< figure src="day-4-col-du-st-martin.jpg" alt="At the top of Col du Saint Martin" width="60%" >}}

On my descent I received a few ice cold rain drops which were very welcome after this hot climb.
Fortunately these were not enough to make the road wet or slippery, so I arrived safely at my accommodation in Roquebilière.

{{< map gpxfile="day-4-to-la-bollene.gpx" show_waypoints="false" >}}

---

## Day 5

The next morning I woke up to tackle the last stage of my journey.
Since my accommodation was immediately at the foot of the [climb to the Col de Turini](https://climbfinder.com/en/climbs/col-de-turini-quartier-boutas), I did not have any opportunity to spin my legs before starting the intense climb: 16 kilometers with an average gradient of 7.2 percent, but in reality most kilometers are either 8 or 9 percent!

{{< figure src="day-5-tdf-sign-col-de-turini.jpg" alt="Tour de France sign for the climb to Col de Turini" width="60%" >}}

While this road is popular for cyclist, it is infamous for rally car racing: every year the *Rally Monte-Carlo* features a *Special Stage* that leads over this col - however in snowy and icy conditions! ([Rallye Monte-Carlo  Onboard Sebastian Loeb [2015]](https://www.youtube.com/watch?v=12gK_2hGo7I))

{{< figure src="day-5-climb-col-de-turini-1.jpg" alt="Quiet road on the way to Col de Turini" width="60%" >}}

Thankfully most of the route is covered by shade since I did this climb in the morning.

{{< figure src="day-5-climb-col-de-turini-2.jpg" alt="Beautiful landscape on the way to Col de Turini" width="60%" >}}

Compared to the other summits I passed in the previous days, the top of the Col de Turini is rather unimpressive and does not offer any views - but that's fine since on the way up and down there are more than enough views onto the beautiful landscape.

{{< figure src="day-5-descent-col-de-turini.jpg" caption="Passing through legendary Rally hairpin turns" width="60%" >}}

I descended to *Sospel* from where I started the next ascent towards the *Col de Castillon*.
While it is a local favorite, in my opinion it's a rather unimpressive climb, but that's to be expected after the epic climbs of the last few days.
After passing this col I started descending once more and noticed how the road traffic got busier - and suddenly I found myself standing at the Mediterranean Sea in *Menton*!

{{< figure src="day-5-menton.jpg" caption="Unexpected sea view in Menton" width="60%" >}}

I have to admit that this view felt a bit surreal after spending so many days seeing nothing but steep mountains and deep valleys - especially since the transition from mountainous landscape to sea coast is very abrupt in this area (you can see in the background of the picture above that the mountains reach all the way to the sea).

Once I started cycling along the coast I was also shocked about the very high humidity of the air - something that I was not used over the last couple of days.

From Menton I circled around (and above) *Monaco* via the *Col d'Eze* to finally arrive in *Nice*, where I got myself a well-deserved, fresh ice cream.

{{< figure src="day-5-nice.jpg" alt="View onto Nice" width="60%" >}}

This area of the world is really impressive because you can be in the mountains while at the same time having incredible views to the sea.

{{< map gpxfile="day-5-to-antibes.gpx" show_waypoints="false" >}}

<!-- Col de Turini (1607m) -->
<!-- Col de Castillon (728m) -->
<!-- Col d'Eze (507m) -->


---

## Day 6: Bonus

Imagine this: you wake up one morning without a plan for the day and the last five days all you have been doing was cycling - what do you do?
Of course, you cycle some more because now you don't know what else to do with your life!



From Antibes I headed into the countryside and towards the mountains. I climbed up to the mountain village *Gourdon*, descended down through the *Gorge du Loup* (a very nice descent!) and finally arrived back at the coast in *Villeneuve-Loubet*, where I found bakery with glorious, huge sandwiches - bon appétit!

{{< figure src="day-6-sandwich.jpg" alt="Glorious sandwich at the beach" width="60%" >}}

{{< map gpxfile="day-7-antibes.gpx" >}}

---

## Day 7: Exploring Nice

On my last day in the area I took a real rest day: I hopped on the train to Nice and wandered through the pedestrian area of the new town, the narrow alleys of the old town and of course along the *Promenade des Anglais*.
Since you came here to read about cycling and not sightseeing, I'll spare you the details. :-)

{{< figure src="day-7-nice.jpg" caption="View over the famous beach promenade in Nice" width="60%" >}}

---

## Conclusion

Overall it was a great experience!
I was lucky to not have any technical (with the bike) or physical (with myself) issues.

I recommend the trip to any cyclist who can bear the thought of being an bike for a week (or more).
If you have any questions about the trip or are planning to do it yourself, feel free to reach out to me.

Happy cycling!
