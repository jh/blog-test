+++
title = "Configure Prometheus on a Sub-Path behind Reverse Proxy"
description = "How to correctly configure Prometheus Web UI on a Sub-Path with Traefik reverse proxy"
tags = ["prometheus", "traefik", "docker"]
categories = "Software"
date = "2020-09-24"
+++

While configuring my new Prometheus and Grafana setup, I had to spend some time figuring out how to get the Prometheus UI to correctly serve on a sub-path, e.g. `example.com/prometheus`.

I won't bore you with all the intermediate steps I tried (and failed!), but rather jump straight to the conclusion: You need to set the `--web.external-url` flag to `/prometheus/`:
```
prometheus --web.external-url=/prometheus/
```

From the [Prometheus manpage](https://manpages.debian.org/testing/prometheus/prometheus.1.en.html):
```

--web.external-url=<URL>
    The URL under which Prometheus is externally reachable (for example, if Prometheus is served via a reverse proxy). Used for generating relative and absolute links back to Prometheus itself. If the URL has a path portion, it will be used to prefix all HTTP endpoints served by Prometheus. If omitted, relevant URL components will be derived automatically.
--web.route-prefix=<path>
    Prefix for the internal routes of web endpoints. Defaults to path of --web.external-url.
```

(*By the way: I'm not sure what's so hard about just documenting the command line flags on the offcial documentation pages instead of [an unhelpful "just run prometheus -h"](https://prometheus.io/docs/prometheus/latest/configuration/configuration/)*)

Then configure the reverse proxy so that it forwards requests for `/prometheus/` to the address Prometheus is listening on, but **without stripping the prefix (!)**.

Traefik v2:
```
traefik.http.routers.<my-router>.rule="Host(`example.com`) && PathPrefix(`/prometheus/`)"
```

Traefik v1:
```yml
prometheus:
  image: prom/prometheus:v2.20.0
  command:
     - '--web.external-url=/prometheus/'
     - '--web.route-prefix=/prometheus/'
  # ...
  deploy:
    labels:
      - traefik.frontend.rule=Host:example.com;PathPrefix:/prometheus/
      - traefik.port=9090
```

Nginx:
```nginx
location /prometheus/ {
    proxy_pass http://localhost:9090;
}
```

Thanks also to [this issue](https://github.com/prometheus/prometheus/issues/4925) for some inspiration.

Hope this helps, happy monitoring!
