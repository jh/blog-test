#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

# define mathematical function
def z(x,y):
    return np.exp(-(np.square(x) + np.square(y))/0.1)

# create initial dataset
x = np.arange(-1,1,0.05)
xy = [(j,k) for j in x for k in x]
out = [z(p[0],p[1]) for p in xy]

# split dataset into training and testing data (also shuffles)
x_train, x_test, y_train, y_test = train_test_split(xy, out)

# draw 3d plot of training and testing data
fig = plt.figure()
ax = fig.gca(projection='3d')

# plot train datapoints
x1_vals = np.array([p[0] for p in x_train])
x2_vals = np.array([p[1] for p in x_train])

ax.scatter(x1_vals, x2_vals, y_train)

# plot test datapoints
x1_vals = np.array([p[0] for p in x_test])
x2_vals = np.array([p[1] for p in x_test])

ax.scatter(x1_vals, x2_vals, y_test, marker='x')

#plt.show()

# set up network with parameters
mlp = MLPRegressor(
    hidden_layer_sizes=[20],
    max_iter=1000,
    tol=0,
)

# train network
mlp.fit(x_train,y_train)

# test
predictions = mlp.predict(x_test)

# calculate error
mse = mean_squared_error(y_test, predictions)
print("MSE:", mse)

# plot predictions
ax.scatter(x1_vals, x2_vals, predictions, c='red')
plt.show()
