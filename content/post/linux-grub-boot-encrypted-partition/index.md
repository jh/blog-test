+++
title = "Boot encrypted Linux from GRUB"
description = "How to boot Linux with an encrypted root partition from GRUB shell"
tags = ["grub","partition","linux"]
categories = "Software"
date = "2018-10-15T10:00:00+02:00"
+++

So I just had a very unpleasent system hang during a kernel upgrade which lead me to being dumped into the GRUB shell - on Sunday evening at 10 pm. And I need my laptop tomorrow...

Well, let's do this!

I have three partitions on my (single) drive:

* `/dev/sda1`: MBR
* `/dev/sda2`: `/boot` partition
* `/dev/sda3`: `/root` partition, encrypted, mounted as `/dev/mapper/sda3_crypt`

First we need to tell GRUB which Linux image (vmlinuz) we'll be using and which partition the kernel mounts as its root partition.
Though this needs to be the encrypted virtual volume, not your actual "physical" root partition.
In my case `/dev/sda3` is the encrypted partition and gets mounted to `/dev/mapper/sda3_crypt` once decrypted.

The partition addressing is a bit weird in GRUB (without implying this is GRUB fault).
Since none of the partition are mounted (yet), all path need to be prefixed with `(DISK,PARTITION)`.
You can view the available partitions with `ls`:
```
grub> ls
(hd0) (hd0,gpt0) (hd0,gpt1) (hd0,gpt2)
```

`hd0` is obviously my first (and only) hard drive.
`gpt{0..2}` refer to partitions 1-3, as I am using a [GUID Partition Table (GPT)](https://en.wikipedia.org/wiki/GUID_Partition_Table).

These are the commands I used to implement the steps described above:

```
# enable paging support in GRUB shell
set pager=1

# load linux image with appropriate parameters
linux (hd0,gpt2)/vmlinuz-4.17.0-3-amd64 root=/dev/mapper/sda3_crypt

# set initramdisk
initrd (hd0,gpt2)/initrd.img-4.17.0-3-amd64

# go!
boot
```

You'll need to adapt the above commands to your environment.
Fortunately, GRUB provides auto-complete support, so you don't have to type out every last letter.

References:

* [ArchWiki: GRUB](https://wiki.archlinux.org/index.php/Grub#Using_the_command_shell)
