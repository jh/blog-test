+++
title = "The Archipelago Trail"
description = "A 200 kilometer bicycle tour across the many islands in front of Turku, Finland. GPX track available as download."
categories = "Outdoor"
tags = ["cycling", "finland"]
date = "2021-07-06"
image = "sunset.jpg"
+++

The Archipelago Trail leads around dozens of the 40.000 islands in front of the Finnish city [Turku](https://en.wikivoyage.org/wiki/Turku).
A friend of mine described this route as "green and blue heaven" and I think the term is pretty accurate: lots of lush forests, lots of sea water.
In addition to cycling over the islands, the route also consists of several ferries used for hopping from island to island.
This provides a nice variety and breaks, which makes the route suitable for people who are not very experienced with long distance cycling.

{{< figure src="beach.jpg" caption="One of the remote beaches along the way" height="350px" >}}

There are two versions of the route: the regular one (with 9 ferries), the extended one (with one ferry less) and a [shortcut one](https://visitparainen.fi/en/the-small-archipelago-trail/).
I cycled the extended one (200km of cycling) in bikepacking-mode (meaning I had my tent, food and everything else with me) in two-and-a-half days.
For experienced cyclists it's definitely also doable in two days.
While the route is quite hilly in some parts, the hills are never that high, so after a short climb you are immediately rewarded with a recovery roll down the hill.
The route is entirely asphalted (except a short piece between Nagu and Pärnäs, which can be avoided by following the main street), thus it is suitable also for city- and road bikes.
The condition of the roads and cycle paths is excellent.

{{< figure src="on-the-ferry.jpg" caption="On the ferry!" height="350px" >}}

In general, towards the end of the route (as you reach the north-western part) you need to be mindful about the ferry schedules: some ferries just leave few times a day, so make sure to check on [finferries.fi](https://www.finferries.fi/en/).
The nice aspect of having a tent with me was that I didn't need to worry about the ferry schedules or where to sleep, instead I could cycle as much as possible or as little as I wanted to -- given enough time and food ;-)

At the end of the trip, Turku is a great place to spend a relaxing evening at the river Aura.

{{< figure src="bike.jpg" caption="Bicycle parked in Turku" height="300px" >}}

Overall, it's a great route to enjoy some of Finland's nature, while still having a glimpse of civilization (so you can occasionally buy food and snacks).
If you are looking for more impressions, head over to the [official page of the archipelago trail](https://visitparainen.fi/en/the-archipelago-trail/).
There you can also find [detailed route descriptions and maps](https://visitparainen.fi/en/?page_id=3772).

{{< figure src="archipelago-trail-sign.jpg" caption="Sign of the Archipelago Trail in Finnish and Swedish" height="300px" >}}

I highly recommend the route to anyone who's into cycling and happens to be in (or close to) Finland.

**Website**: [Visit Parainen - The Archipelago Trail](https://visitparainen.fi/en/the-archipelago-trail/)

**Total length**: 200 km

**Total elevation**: 1550 m

{{< map gpxfile="archipelago-trail.gpx" >}}

Happy cycling!
