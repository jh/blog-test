+++
categories = "Software"
tags = ["android"]
title = "Android Apps"
date = "2019-02-27T17:50:52+01:00"
draft = false
description = "Android Apps I use and recommend"
+++

I have been meaning to write about the Android Apps I use on my phone, but everytime I start writing about it I think to myself "Nah, everyone whose interested in these apps probably knows about them anyway".
But then I talk to people about what apps they like to use and I'm surprised to find out about apps I have never heard of (and the other person, too!).

So here we are: I'm writing about the Android apps I use which I consider interesting for others.
(I'm not listing all my installed apps)

At the time of writing (February 2019) I'm using [LineageOS 15.1 (based on Android 8.1) with microG]({{< ref "mi-a1-lineage" >}}).

-------

### AnkiDroid

* Homepage: https://github.com/ankidroid/Anki-Android
* License: GPL-3.0
* App ID: `com.ichi2.anki`

AnkiDroid is an app for learning flashcards.
You can either download pre-made decks from AnkiWeb or create your own.
I primarily use it to learn interesting English vocabulary I stumble across while reading or listening, since English is not my native language.
AnkiDroid automates all the sorting and shuffling you have to do with an analog flashcard box.
It also reminds you when to study and repeat.

<figure>
    <img src="ankidroid_1.png" style="max-height: 500px;">
</figure>


### AntennaPod

* Homepage: https://antennapod.org
* License: MIT
* App ID: `de.danoeh.antennapod`

If you listen to Podcasts on Android, AntennaPod is the app you should use.
It features an excellent UI and UX, can fetch podcasts from many different sources (RSS/Atom, iTunes, gpodder, FYYD) and just generally has a lot of useful and neat features.
I also really like that it has a dedicated function to export the database (subscribes podcasts, episodes listened to, ...) - this makes backups very easy.

<figure>
    <img src="antennapod_1.png" style="max-height: 500px;">
    <img src="antennapod_2.png" style="max-height: 500px;">
</figure>

### AuroraStore

* Homepage: https://gitlab.com/AuroraOSS/AuroraStore
* License: GPL-2.0
* App ID: `com.dragons.aurora`

While I try to use as many open source apps as possible (via the F-Droid App Store), sometimes you simply need a vendor-specific app or there is just no equivalent FOSS version available.
For these cases I use the Aurora Store. It allows you to install apps from Google's Play Store either with a shared, "anonymous" account or your own account (the latter is required for installing previously purchased apps).
Most importantly: without the need to install the entire Play Services suite.
I have configured an automatic whitelist of apps so that Aurora will only update the apps it installed (and not overwriting installations by F-Droid).

### Bats! HIIT

* Homepage: https://github.com/kwantam/batsHIIT
* License: MIT
* App ID: `org.jfet.batsHIIT`

This is my workout timer app of choice.
You can easily configure workout, break and rest intervals (as well as their amount) for [high intensity interval training](https://en.wikipedia.org/wiki/High-intensity_interval_training).
Additionally, these profile can also be saved and loaded later again which is quite handy.

<figure>
    <img src="batshiit_1.png" style="max-height: 500px;">
</figure>

### Conversations

* Homepage: https://conversations.im/
* License: GPL-3.0
* App ID: `eu.siacs.conversations`

The top dog of mobile XMPP messengers.
For most things Conversations can be treated pretty much like any other messenger.
The UI design is very good, thus making onboarding of new users frictionless.

Though I don't agree with all UX decision from the author (e.g. messages can just silently fail without notifying the sender), therefore I also want to give a shoutout to [Pix-Art Messenger](https://jabber.pix-art.de/) (a fork of Conversations).

### Simple Gallery

* Homepage: https://simplemobiletools.github.io/
* License: Apache-2.0
* App ID: `com.simplemobiletools.gallery.pro`

It is kind of sad that I have to install a third party app for viewing images on Android, but the ASOP gallery app is just ridiculous since it groups pictures by date taken (instead of folders) in the main view - it's called a "Timeline".

Here is the worst part: it can't be changed!!!
I am not going to click the "Albums" button each time I open the app - no thank you. (*In fact the app does not have any settings at all.*)

Therefore I'm using Simple Gallery (part of the simple tools suite) as a gallery replacement and I'm pretty happy with it.
Despite its name it's not such a simple app, there are a lot of options to customize the behavior.
I like to have my entire image collection sorted by folders ("Camera", "Conversation Pictures", " Screenshots", ...) and the images inside these are then sorted in reversed chronological order by date taken (the most recent image ends up at the top).

<figure>
    <img src="simplegallery_1.png" style="max-height: 500px;">
</figure>


### Meteogram

* Homepage: https://meteograms.com/
* License: Proprietary
* App ID: `com.cloud3squared.meteogram.pro`

In my opinion the best weather app out there. Highly recommended!

It takes a while set up (the amount of customization options is truly astronomic) but once I have tweaked it just the way I like it I get a very good "feel" for the weather over the next few days.
It also lets you select from a dozen different weather service providers which is handy for different regions of the world or if you're simply unhappy with the accuracy of the forecasts.

Once you have put in the effort to customize it (trust me, you will like the results) you can also import and export these settings. Again making it easy to create backups.

<figure>
    <img src="meteogram_1.png" style="max-height: 500px;">
</figure>

### MuPDF Viewer

* Homepage: https://mupdf.com/
* License: GPL-3.0
* App ID: `com.artifex.mupdf.viewer.app`

The dumbest, fastest and most reliable FOSS document reader out there.
It supports a bunch of different formats (PDF, XPS, OpenXPS, CBZ, EPUB, and FictionBook 2), but for me the most important feature is simply its speed.
I don't want to wait multiple seconds before the app has launched to then start rendering the PDF. With MuPDF this is just instant.

### OI Shopping

* Homepage: http://www.openintents.org/shoppinglist/
* License: Apache-2.0
* App ID: `org.openintents.shopping`

This is the 21st century, so of course I'm using a digital shopping list.
If you haven't tried it definitively give it a go: you carry your phone with you all day anyway, so it's super easy to just add items that spring to mind during the day.
Then in the grocery store you can simply check one item after the other off the list.

<figure>
    <img src="oishoppinglist_1.png" style="max-height: 500px;">
</figure>

### Packlist

* Homepage: https://github.com/nbossard/packlist
* License: Apache-2.0
* App ID: `com.nbossard.packlist`

Yes, more lists! A very basic application for creating trip packing lists.
The use case is very similar to the shopping list mentioned above.

<figure>
    <img src="packlist_1.png" style="max-height: 500px;">
</figure>

### SatStat

* Homepage: https://mvglasow.gitlab.io/satstat/
* License: GPL-3.0+
* App ID: `com.vonglasow.michael.satstat`

An application to monitor the status of GNSS, phone sensors and radio network.
Can be handy to debug connectivity issues or just for the curious.

<figure>
    <img src="satstat_1.png" style="max-height: 500px;">
    <img src="satstat_2.png" style="max-height: 500px;">
    <img src="satstat_3.png" style="max-height: 500px;">
</figure>

### Send Reduced

* Homepage: https://github.com/arpruss/sendreduced
* License: Apache-2.0
* App ID: `mobi.omegacentauri.sendreduced`

This is a really useful app that has a minimally invasive design and is well implemented.
Due to the resolution of modern camera sensors the picture sizes can be quite large which can become a problem when you want to send these via e-mail or upload them somewhere.
SendReduced acts as a middle-man to which you "Share" the pictures.
SendReduced then compresses them according to the desired parameters and automatically opens another "Share" dialog where you can select the destination app (e.g. E-Mail, Messenger, ...)

<figure>
    <img src="sendreduced_1.png" style="max-height: 500px;">
</figure>

### NewPipe

* Homepage: https://newpipe.schabi.org/
* License: GPL-3.0+
* App ID: `org.schabi.newpipe`

The better YouTube app. NewPipe allows you to play videos in the background, download video and/or audio and subscribe to channels - all without the need for a Google account.
Additionally it supports playing music from SoundCloud, too.
I consider the fact that it doesn't show any comments a feature.

NewPipe not available through the Play Store since it violates Googles Term of Service. It is NOT illegal however.

<figure>
    <img src="newpipe_1.png" style="max-height: 500px;">
    <img src="newpipe_2.png" style="max-height: 500px;">
</figure>

## Organizer

I group the following apps into a common "Organizer" category because they all work together.
The basis forms my selfhosted Nextcloud instance which exposes CardDAV and CalDAV endpoints for synchronizing contacts and calendar events, respectively.

* [DAVx5](https://www.davx5.com/) (GPL-3.0): `at.bitfire.davdroid`
* [Etar](https://github.com/Etar-Group/Etar-Calendar) (Apache-2.0): `ws.xsoh.etar`
* [OpenTasks](https://opentasks.app/) (Apache-2.0): `org.dmfs.tasks`
* [ICSx5](https://icsx5.bitfire.at/) (GPL-3.0): `at.bitfire.icsdroid`

DAVx5 synchronizes (bidirectional) the local calendar and contacts with the remote ones.
The calendar synchronization also includes tasks.
With ICSx5 I fetch remote ICS calendars.

For viewing and editing contacts I use the default ASOP Contacts app, for calendar events I use Etar and for tasks I use OpenTasks.

<figure>
    <img src="davx5_1.png" style="max-height: 500px;">
</figure>

## Other

The apps below are all pretty well known, so I don't see much point in explaining them.
Nevertheless I want to give them their honorable mention, since these are all apps I quite like and use regularly.

Name     | Description | License
:-------:|-------------|:---------:
[K-9 Mail](https://k9mail.github.io/) | Best E-Mail Client for Android, together with OpenKeyChain supports PGP Mails | Apache-2.0
[OpenKeyChain](https://www.openkeychain.org/) | OpenPGP for Android | GPL-3.0
[OsmAnd](https://osmand.net/) | Most comprehensive Mapping and Routing app based on OpenStreetMaps | GPL-3.0
[Nextcloud](https://github.com/nextcloud/android) | App for connecting to your Nextcloud instance | GPL-2.0
[F-Droid](https://f-droid.org/) | Free Software App Store | GPL-3.0
[Firefox (Fennec)](https://www.mozilla.org/en-US/firefox/mobile/) | Fast and secure browsing (even on mobile) thanks to Addons uBlock Origin and HTTPS Everywhere | MPL
[Firefox Focus](https://www.mozilla.org/en-US/firefox/mobile/) | For short, non-persistent browsing sessions | MPL-2.0
[VLC](http://www.videolan.org/vlc/download-android.html) | Still the most versatile media player around | GPL-3.0
[Wikipedia](https://www.mediawiki.org/wiki/Wikimedia_Apps) | The entire human knowledge available in your pocket | Apache-2.0
[Sky Map](http://sky-map-team.github.io/stardroid/) | Point your phone at the sky and let AR show you the stars! | Apache-2.0
[Nextcloud Notes](https://github.com/stefan-niedermann/nextcloud-notes) | Client for Nextcloud Notes App | GPL-3.0+

## Input Languages

For people who regularly write in multiple languages with their phone (for me German and English), I also recommend setting up multiple input languages.
Go to `Settings -> System -> Languages & Input -> Languages` and add the ones you need.
Then a little globe will appear next to the space key allowing you to easily switch languages (both spell check & auto correct as well as the keyboard layout)
The spacebar will also show the currently selected language.

<figure>
    <img src="languagesettings_1.png" style="max-height: 500px;">
</figure>

-------

Finally, here is my (current) homescreen:

<figure>
    <img src="homescreen_1.png" style="max-height: 800px;">
</figure>

I would like to thank all authors of the aforementioned applications for their work in developing and maintaining these applications.

Do you have any other apps about which you are glad they exists? Let me know!
