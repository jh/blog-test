+++
title = "Go: Debugging why ParseMultipartForm returns Error \"no such file or directory\""
description = "Going down the rabbit hole of debugging a failing multipart upload on a Go server to finally figuring out that the /tmp directory is missing inside a minimal Docker container image"
categories = "Software"
tags = ["go", "mime", "multipart"]
date = "2020-09-29"
+++

For my most recent project [u9k](https://u9k.de), I implemented a simple file upload API endpoint that accepts files as part of a `multipart/form-data` request.
As usual, my implementation worked *perfectly fine on my local machine* (haha), but after deploying it to my server (wrapped up inside a [minimal Docker container](https://git.cubieserver.de/jh/u9k/src/commit/8beeb5b3f1dbb977e4b459e4de072f4f10a1cd96/Dockerfile)), it started to sporadically fail - especially for large files:

```
2020/09/29 05:57:58 "POST http://u9k.de/file/ HTTP/1.1" from 10.0.0.34:59326 - 400 49B in 8.554993597s
2020/09/29 05:57:58 Failed to parse form: open /tmp/multipart-855377186: no such file or directory
```

The error message is generated from the following [piece of code](https://git.cubieserver.de/jh/u9k/src/commit/8beeb5b3f1dbb977e4b459e4de072f4f10a1cd96/api/helpers.go#L102):
```go
	// parse uploaded data
	err := r.ParseMultipartForm(int64(10 << 20)) // 32 MB in memory, rest on disk
	if err != nil {
		log.Printf("Failed to parse form: %s\n", err)
		return nil
	}
```

Sooo, what's happening here? To find out, I started going through the call graph.

Internally, [ParseMultipartForm](https://github.com/golang/go/blob/go1.15.2/src/net/http/request.go#L1277) creates a new [MultipartReader](https://golang.org/pkg/mime/multipart/#Reader) and calls its `ReadForm` method:
```go
    mr, err := r.multipartReader(false)
	if err != nil {
		return err
	}

	f, err := mr.ReadForm(maxMemory)
	if err != nil {
		return err
	}
```

[ReadForm](https://github.com/golang/go/blob/go1.15.2/src/mime/multipart/formdata.go#L30) does the actual work of reading, parsing and validating the bytes sent by the client.
Further down in that method is the following code:
```go
// ReadForm parses an entire multipart message whose parts have
// a Content-Disposition of "form-data".
// It stores up to maxMemory bytes + 10MB (reserved for non-file parts)
// in memory. File parts which can't be stored in memory will be stored on
// disk in temporary files.

...
	if n > maxMemory {
			// too big, write to disk and flush buffer
			file, err := ioutil.TempFile("", "multipart-")
			if err != nil {
				return nil, err
			}
			size, err := io.Copy(file, io.MultiReader(&b, p))
			if cerr := file.Close(); err == nil {
				err = cerr
			}
			if err != nil {
				os.Remove(file.Name())
				return nil, err
			}
            ...
    }
```

In case the multipart data does not fit in the specified memory size, the data is written to a temporary file on disk instead.
This file is created with `ioutil.TempFile` (line 11).

Looking at the [documentation of ioutil.TempFile](https://godoc.org/io/ioutil#TempFile) does not reveal anything interesting or suspicious, but the [source code](https://github.com/golang/go/blob/go1.15.2/src/io/ioutil/tempfile.go#L51) gives the next hint:

```go
if dir == "" {
		dir = os.TempDir()
}
```


Well, let's look at [the documentation and code](https://github.com/golang/go/blob/go1.15.2/src/os/file.go#L378) of `os.TempDir` then:
```go
// TempDir returns the default directory to use for temporary files.
//
// On Unix systems, it returns $TMPDIR if non-empty, else /tmp.
// On Windows, it uses GetTempPath, returning the first non-empty
// value from %TMP%, %TEMP%, %USERPROFILE%, or the Windows directory.
// On Plan 9, it returns /tmp.
//
// The directory is neither guaranteed to exist nor have accessible
// permissions.
func TempDir() string { ... }
```

**Aha!**

> "The directory is neither guaranteed to exist nor have accessible permissions."

That's why the open call fails with "no such file or directory", but only inside my minimal Docker container.
Except for the server binary and a few static assets, the container image does not have any directory structure, so `/tmp` does not exist yet.
Subsequently, the `os.OpenFile` call in `ioutil.TempFile` fails and propagates all the way up to the multipart parser and my code!

This was quite a rabbit hole.

Under usual circumstances, `/tmp` exists in any UNIX environment, but to fix this behavior in minimal containers, I added the following [code to my startup routine](https://git.cubieserver.de/jh/u9k/src/commit/8beeb5b3f1dbb977e4b459e4de072f4f10a1cd96/cmd/server/main.go#L50).
It ensures the directory returned by `os.TempDir` exists and has the [correct permissions for a temporary directory](https://unix.stackexchange.com/questions/71622/).

```go
	// make sure we have a working tempdir, because:
	// os.TempDir(): The directory is neither guaranteed to exist nor have accessible permissions.
	tempDir := os.TempDir()
	if err := os.MkdirAll(tempDir, 1777); err != nil {
		log.Fatalf("Failed to create temporary directory %s: %s", tempDir, err)
	}
	tempFile, err := ioutil.TempFile("", "genericInit_")
	if err != nil {
		log.Fatalf("Failed to create tempFile: %s", err)
	}
	_, err = fmt.Fprintf(tempFile, "Hello, World!")
	if err != nil {
		log.Fatalf("Failed to write to tempFile: %s", err)
	}
	if err := tempFile.Close(); err != nil {
		log.Fatalf("Failed to close tempFile: %s", err)
	}
	if err := os.Remove(tempFile.Name()); err != nil {
		log.Fatalf("Failed to delete tempFile: %s", err)
	}
	log.Printf("Using temporary directory %s", tempDir)
```
