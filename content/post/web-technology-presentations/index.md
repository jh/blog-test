+++
title = "Web Technology Presentations"
description = ""
tags = ["web","html","javascript","css","php","mysql","mvc"]
categories = "Software"
date = "2019-06-27T18:00:00+01:00"
+++

<style>
/* CSS padding-top trick, from http://www.fredparke.com/blog/css-padding-trick-responsive-intrinsic-ratios */
.wrapper-ratio-169 {
  position: relative;
  padding-bottom: 56%;
  height: 0;
}
.element-to-stretch {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: teal;
}
</style>

For a course I took at university this semester, I had to create simple 10 minute presentation for the topics CSS, JavaScript, PHP, MySQL and MVC.

I think these provide a quite nice introduction to these topics.

## CSS + HTML

<div class="wrapper-ratio-169">
    <iframe class="element-to-stretch" src="/web-technology-presentations/html-css.html"></iframe>
</div>

## JavaScript + HTML

<div class="wrapper-ratio-169">
    <iframe class="element-to-stretch" src="/web-technology-presentations/html-javascript.html"></iframe>
</div>

## PHP

<div class="wrapper-ratio-169">
    <iframe class="element-to-stretch" src="/web-technology-presentations/php-html.html"></iframe>
</div>

## MySQL

<div class="wrapper-ratio-169">
    <iframe class="element-to-stretch" src="/web-technology-presentations/mysql-php.html"></iframe>
</div>

## MVC

<div class="wrapper-ratio-169">
    <iframe class="element-to-stretch" src="/web-technology-presentations/mvc-php.html"></iframe>
</div>

---

All presentations are licensed under [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

I used [RemarkJS](https://remarkjs.com/) to create these presentations:
"A simple, JavaScript-based, Markdown-driven slideshow tool targeted at people who know their way around HTML and CSS".

I especially like the [presentation mode](https://github.com/gnab/remark/wiki/Presentation-mode) of RemarkJS which allows you to preview the next slide, comments and a stop watch.
Just press "P" after focusing on one of the above presentations.
