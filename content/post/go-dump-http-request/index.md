+++
Description = "Print all Headers and Content of HTTP Requests with Go"
date = "2016-12-26T20:35:22+01:00"
title = "Go: Dump HTTP Request"
draft = false
tags = ["go", "net/http", "http"]
categories = "Software"
+++

Recently, [I was implementing](/2018/continuous-blog-deployment-self-hosted-edition/) a Webhook for [Gogs](https://gogs.io/) in [Go](https://golang.org). Unfortunately, the documentation is not one hundred percent complete and could be a bit more specific at times.

Also, neither reading [Gogs source code](https://github.com/gogits/gogs/blob/master/models/webhook.go) nor other [people's implementation](https://github.com/kanboard/plugin-gogs-webhook) is what anyone really wants here.

What I ended up doing was dumping the complete request (including all HTTP Headers and Body) with the following, simple Go program:

```go
package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		dump, err := httputil.DumpRequest(r, true)
		if err == nil {
			fmt.Printf("%s\n", dump)
		} else {
			fmt.Printf("%s\n", err)
		}
		return
	})
	http.ListenAndServe(":8080", nil)
}
```

Instead of manually ranging over all values in the [Header string map](https://golang.org/pkg/net/http/#Header) and doing all sorts of other stuff, simply use the [DumpRequest function](https://golang.org/pkg/net/http/httputil/#DumpRequest) from the [net/http/httputil package](https://golang.org/pkg/net/http/httputil/). There are also [DumpRequestOut](https://golang.org/pkg/net/http/httputil/#DumpRequestOut) (for requests you are going to send out) and [DumpResponse](https://golang.org/pkg/net/http/httputil/#DumpResponse) (for received responses) functions available.

Because these functions work with all HTTP versions, they are especially valuable for debugging HTTP/2 connections, since you get simple string output (instead of bytes).

Happy hacking!
