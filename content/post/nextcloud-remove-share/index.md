+++
title = "Remove Nextcloud Share from Database"
description = "How to forcefully remove a nextcloud share from the MySQL database"
tags = ["nextcloud", "share", "mysql", "database"]
categories = "Software"
date = "2018-03-11T18:57:00+01:00"
+++

My Nextcloud instance is currently suuuuper slow, because one of the federated Nextcloud instances that *has shared a file with me* is offline. Apparently, [this issue has been addressed a long time ago](https://github.com/nextcloud/server/issues/2638), but I'm still having this problem.

When I try to "unshare" the file in the Web GUI, I simply get (after a looong wait) the error message `Error deleting file "xyz".`. The HTTP DELETE operation sent to the backend returns with a 503 Service Unavailable, as well as some WebDAV exception in XML.

Because I got quite annoyed by this (it outright made the Web interface unusable), I decided to take it into my own hands and forcefully remove the entry from the database. Easier said than done, as Nextcloud's database and table structure is quite complex.

```
MariaDB [nextcloud]> show tables;
+--------------------------------+
| Tables_in_nextcloud            |
+--------------------------------+
| oc_accounts                    |
| oc_activity                    |
| oc_activity_mq                 |
| oc_addressbookchanges          |
| oc_addressbooks                |
| oc_admin_sections              |
| oc_admin_settings              |
| oc_appconfig                   |
| oc_audioplayer_albums          |
| oc_audioplayer_artists         |
| oc_audioplayer_genre           |
| oc_audioplayer_playlist_tracks |
| oc_audioplayer_playlists       |
| oc_audioplayer_stats           |
| oc_audioplayer_streams         |
| oc_audioplayer_tracks          |
| oc_authtoken                   |
| oc_bookmarks                   |
| oc_bookmarks_tags              |
| oc_bruteforce_attempts         |
| oc_calendarchanges             |
| oc_calendarobjects             |
| oc_calendarobjects_props       |
| oc_calendars                   |
| oc_calendarsubscriptions       |
| oc_cards                       |
| oc_cards_properties            |
| oc_comments                    |
| oc_comments_read_markers       |
| oc_credentials                 |
| oc_dav_shares                  |
| oc_documents_invite            |
| oc_documents_member            |
| oc_documents_op                |
| oc_documents_revisions         |
| oc_documents_session           |
| oc_external_applicable         |
| oc_external_config             |
| oc_external_mounts             |
| oc_external_options            |
| oc_federated_reshares          |
| oc_file_locks                  |
| oc_filecache                   |
| oc_files_trash                 |
| oc_flow_checks                 |
| oc_flow_operations             |
| oc_group_admin                 |
| oc_group_user                  |
| oc_groups                      |
| oc_jobs                        |
| oc_ldap_group_mapping          |
| oc_ldap_group_members          |
| oc_ldap_user_mapping           |
| oc_mimetypes                   |
| oc_mounts                      |
| oc_news_feeds                  |
| oc_news_folders                |
| oc_news_items                  |
| oc_notes_meta                  |
| oc_notifications               |
| oc_oauth2_access_tokens        |
| oc_oauth2_clients              |
| oc_podcasts_episodes           |
| oc_podcasts_feeds              |
| oc_polls_comments              |
| oc_polls_dts                   |
| oc_polls_events                |
| oc_polls_notif                 |
| oc_polls_particip              |
| oc_polls_particip_text         |
| oc_polls_txts                  |
| oc_preferences                 |
| oc_privatedata                 |
| oc_properties                  |
| oc_schedulingobjects           |
| oc_share                       |
| oc_share_external              |
| oc_storages                    |
| oc_systemtag                   |
| oc_systemtag_group             |
| oc_systemtag_object_mapping    |
| oc_trusted_servers             |
| oc_twofactor_backupcodes       |
| oc_users                       |
| oc_vcategory                   |
| oc_vcategory_to_object         |
+--------------------------------+
86 rows in set (0.00 sec)
```

I'm using Nextcloud 12.0.5 with MariaDB 10.1.

So log into your MySQL instance, select the appropriate database and drop tables -- after backing up your database, of course!

There seems to be something interesting in table `oc_mounts`:
```
MariaDB [nextcloud]> select * from oc_mounts;
+----+------------+---------+---------+---------------------------+----------+
| id | storage_id | root_id | user_id | mount_point               | mount_id |
+----+------------+---------+---------+---------------------------+----------+
| .p.. more entries here ...                                                  |
| 36 |         33 |    6597 | user1   | /user1/files/filexyz/     |     NULL |
+----+------------+---------+---------+---------------------------+----------+
```

But also `oc_share_external` shows a hit:
```
MariaDB [nextcloud]> select * from oc_share_external;
+----+-------------------------------+-----------+-----------------+----------+----------+---------+-------+-----------+----------------------------------+----------+
| id | remote                        | remote_id | share_token     | password | name     | owner   | user  | mountpoint | mountpoint_hash                  | accepted |
+----+-------------------------------+-----------+-----------------+----------+----------+---------+-------+-----------+----------------------------------+----------+
|  6 | https://remote-nextcloud.com/ |         5 | jasdlk49wJSD92A |          | /filexyz | user1   | user2 | /filexyz  | 47f19b20f09d33e4abc4166d611e35b7 |        1 |
+----+-------------------------------+-----------+-----------------+----------+----------+---------+-------+-----------+----------------------------------+----------+
```

So let's drop those rows and see what happens:
```mysql
DELETE FROM oc_share_external WHERE id = 6 LIMIT 1;
Query OK, 1 row affected (0.00 sec)
```

```mysql
DELETE FROM oc_mounts WHERE id = 36 LIMIT 1;
Query OK, 1 row affected (0.00 sec)
```

Quick test `Ctrl-Shift-R` -- everything is plenty fast again!
