+++
title = "Phishing attack with embedded chat tabs in Microsoft Teams"
description = "A phishing attack using the embedded chat tabs feature of Microsoft Teams to steal confidential information and credentials"
tags = ["microsoft", "teams", "phishing", "security", "vulnerability"]
categories = "Software"
date = "2020-05-16"
+++

At the end of May 2020, the 2nd annual [SECCLO summer school](https://secclo.eu/programme/summer-school/) took place, this year with a focus on Cyber Security and - due to the coronavirus situation - online, of course.

We were given the task to investigate security properties and identify possible vulnerabilities in commonly used video chat tools, such as Zoom and Microsoft Teams.

Especially Microsoft Teams is a huge applications and offers many diverse features, not just video calling.
The entire application is basically just a big Electron app.
That's also the reason Microsoft can provide exactly the same app online (at `teams.microsoft.com`).
Subsequently, it uses many web technologies to display content and exchange information.

One interesting features of Teams is the ability to [add "Tabs" to your chats](https://docs.microsoft.com/en-us/microsoftteams/built-in-custom-tabs).
Just like browser tabs, these allow grouping together multiple related items.
The default tab is the chat tab, other examples of tabs include files, presentations, notes etc.:

![Pictures of MS Teams Tabs Options](teams-tabs.png)

This seems like a really useful feature, since you can immediately share any document as a tab with your teammates.
I'm sure Microsoft did their best to isolate these documents from the rest of the application as much as possible, to avoid code execution and other attacks.
Nevertheless, due to the amount of supported formats, I'm sure one could find a vulnerability somewhere.

But that is not the direction I decided to investigate.
Tabs also have the option to display web pages.
In that case, the tabs serve the same purpose as the tabs in your web browser.

Since Teams itself is basically just a browser window, the web page content is directly and seamlessly embedded into the app (Electron / Chromium calls this a [webview](https://www.electronjs.org/docs/tutorial/web-embeds), essentially it works like an iframe element).

<center>
![A regular web browser tab, including URL bar](urlbar.png)
</center>

Unlike your web browser, Teams does not directly show the URL of a tab to the user.
Furthermore, Teams does not even show to the user that the displayed content is external, i.e. does not belong to Teams itself.

One way this can be abused is by re-implementing one of the official Microsoft or Teams web pages and trick the user into providing some kind of confidential information.

As a proof of concept, I choose the Microsoft Login page that gets shown upon loading Microsoft Teams.
As such, it is familiar to any Teams user.

If we can convince another user to open the tab we added, because he or she should look at "Our Awesome Website", the user will presented with the Microsoft Teams login screen.
After the user entered the credentials, we can the redirect to the page that initially drew the users attention.

![Demo PoC Phishing Attack](spoofed-login-page.png)

I have reported this vulnerability to Microsoft Security Response Center ([VULN-021910](https://msrc.microsoft.com/report-detail/VULN-021910)).
[According to their criteria](https://technet.microsoft.com/library/cc751383.aspx), this issue does not classify security vulnerability:

> Unfortunately your report appears to rely on social engineering to accomplish, which would not meet the bar for security servicing.

I fully agree that the vulnerability depends entirely on social engineering / phishing.
However, in this case Teams also makes it especially easy to perform this kind of attack, since the user is not presented with any indication that he or she is interacting with an external website.
Especially now, when there is a huge influx of non-technical users for online collaboration tools like Teams, I definitely see this as a major flaw.

You might argue that the attacker must already have access to the chat to embed the tab, anyway.
However, at the same time you can not assume that everyone in a chat is trusted: think about schools and universities, or companies inviting externals to their chats.

As a remedy, Teams should display the URL bar or visually distinguish the app from external components (e.g. with borders).
