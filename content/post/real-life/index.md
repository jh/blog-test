+++
title = "Real Life"
description = "DIY Hacking in Real Life"
tags = ["real", "life", "diy"]
categories = "DIY"
date = "2018-06-29T23:00:00+02:00"
+++

> Judge: When was the first time you met in real life?
>
> Peter Sunde: We don't use that expression. We say AFK - we think the internet is for real.

My blog mostly deals with software and hardware, i.e. the stuff computers are made of.
And though one of my favorite quotes addresses how real the internet (the construct a network of computers forms) is, sometimes you really have to do something in "real life".

It does not have to be anything exceptional - you do anything with your bare hands.

For instance, tying some ropes to a carabiner and a tire and hanging it on a tree.

![DIY Tire Swing](diy-tire-swing.jpg)

Or follow the advice of this old Chinese proverb:

> The best time to plant a tree was 20 years ago. The second best time is now.

![Pear Tree](pear-tree.jpg)
