+++
date = "2018-07-04T11:00:00+02:00"
title = "Motivation"
tags = ["diy", "motivation", "poster"]
categories = "DIY"
+++

Sometimes you just need some motivation - in the morning, during the day or in the evening.

Here, take this poster:

![Motivation Poster PNG](preview.png)

[Motivation Poster SVG](drawing.svg),
[Motivation Poster PDF](drawing.pdf),
Licensed under [CC-0 (Public Domain)](https://creativecommons.org/publicdomain/zero/1.0/legalcode).

These are the quotes used:

> Discipline is Greater than Motivation. -- Elliot Hulse


> No pain, no gain. -- Arnold Schwarzenegger


> Success is not about the money, the fame or the big house. It's about becoming THE BEST VERSION OF YOURSELF. -- Bryan Burton


> NEVER DOUBT YOURSELF - doubt kills more dreams than failure ever will. -- William Hollis


> Thoughts influence actions. -- Elliot Hulse


> Legends aren't born - they're built. -- Arnold Schwarzenegger


> There is nothing complicated, it only depends on how familiar you are with it. -- Albert Einstein


> The safest way to success is trying it one more time. -- Thomas Edison


> Discipline is remembering what you want. -- David Campbell


> Misery is comfortable, Happiness takes effort. -- David Wong


> While you are killing time, time is killing you. -- Ashley Zahabian


> Some people want it to happen, some wish it would happen, others make it happen. -- Michael Jordan


> Behind every fear is a person you wanna be. -- Andy Frisella


> Do it again, and again, and again. -- Ashley Zahabian


> Damn it, you have to kill those excuses - or they will kill you.


> You don't start training when you're in olympic form - you train to get into olympic form. -- Zbyhnev


Background Photograph by Patrick Tomaso: https://unsplash.com/photos/QMDap1TAu0g

*I had  this poster printed on a 120 cm x 80 cm sheet at one of the various online printing businesses for 30 bucks.*
