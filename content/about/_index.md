+++
title = "About Me"
description = "A short biography and other details about me"
lastmod = "2021-10-29"
+++

Hi, I'm Jack.

I like building IT infrastructure and cycling.

On this blog I share [insights related to software](/categories/software/) as well as my [outdoor experiences](/categories/outdoor/) -- and also some things in between.

## Short Bio

I currently live in Geneva, Switzerland, where I work at the [CERN research organization](https://home.cern) as an OpenShift Kubernetes administrator and developer.

Professionally, my main interest is building secure, reliable and maintainable software systems.
This includes automating tasks to ease human-machine interaction, experimenting with old and new concepts and "doing things properly".

My special areas of interest are systems performance, observability and efficiency.
Practically, I find myself between development and operations: I like writing software, but if there is already a piece of open-source software that implements it, I prefer integrating those components.
Naturally, I am a proponent of DevOps culture.
I have a strong bias towards Infrastructure-as-Code and documentation.

In my spare time I enjoy cooking, cycling, hiking and practicing calisthenics.
From time-to-time I will also throw in a trail run to mix things up a bit.

I also run my own server for several applications: the blog you are reading right now, Nextcloud (File sharing and groupware), Gitea (Git hosting), ejabberd (XMPP chat) among others.
This hobby allows me to practice my skills and experiment with new technologies, such as Kubernetes, Pulumi, Prometheus and others.

I am an advocate of Free, Libre and Open-source software.

<figure style="margin-bottom: 1.5em;"><figcaption>
    <h4><i><center>
    Camping at lake Brombachsee, Germany (left).
    <br>
    Junction Hackathon at Aalto University, Finland (right).
    </center></i></h4>
  </figcaption>
  <img src="20200624_075451_min.jpg" style="object-fit: contain; width: 49%;" loading="lazy">
  <img src="IMG_20191117_100335446_min.jpg" style="object-fit: contain; width: 49%;" loading="lazy">
</figure>

## Professional

Between February and August 2021 I was a Thesis worker in the Security Solutions team of [Ericsson Finland](https://www.ericsson.com/en).
Apart from writing my thesis on the topic of [Dimensioning, Performance and Optimization of Cloud-native Applications](https://aaltodoc.aalto.fi/handle/123456789/109318), I was also engaged in the daily activities of software development and server administration.

From January until August 2020 I have been working at [Kodit.io](https://kodit.io/en/company/) as a Junior DevOps Engineer, where I focused on backend software development (Python) and CI/CD.

From 2015 to 2019 I have been working at [Fraunhofer-Allianz Vision](https://www.vision.fraunhofer.de/en.html) as a student assistant for administration of Linux systems.

In 2017 I have completed an internship at [Nokia Solutions and Networks](https://www.nokia.com/networks/), where I evaluated two performance monitoring frameworks (SystemTap and Intel PT).

## Academic

In 2021, I completed my Master's degree in [Security and Cloud Computing](https://secclo.eu) at [Aalto University](https://www.aalto.fi/en) (Espoo, Finland) and [EURECOM](https://www.eurecom.fr) (Sophia-Antipolis, France).
My [Master's thesis](https://aaltodoc.aalto.fi/handle/123456789/109318) covered the topic of cloud-native applications with a particular focus on autoscaling.
For my Bachelor's degree I studied Information and Communication technology (a mix between electrical engineering and computer science) at [Friedrich-Alexander University](https://www.fau.eu) (Erlangen, Germany).

[Here](/publications/) you can find an overview of my publications.

## Elsewhere

You can also find me on [GitHub](https://github.com/jacksgt) and [LinkedIn](https://www.linkedin.com/in/jack-henschel).

## PGP Keys

I am happy to receive end-to-end encrypted e-mails.

Fingerprint: `E5235F5B9415A2BB760BBF185791D0FACE0AF03C`

Get the public key from your favorite keyserver or download it [here](/about/jacksgt.pubkey.asc).

{{< figure src="20210710_215827_min.jpg" title="Hiking in Nuuksio National Park, Finland." width="100%" >}}
